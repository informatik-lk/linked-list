import java.util.Objects;

public class List<T> {
    private Node<T> first, current;

    public List() {
        this.first = null;
        this.current = null;
    }
    
    public List(T obj) {
        this.first = new Node<T>(obj);
        this.current = this.first;
    }

    public int getLength() {
        if (this.isEmpty()) return 0;

        int counter = 0;

        Node<T> currentIndex = this.current;

        this.toFirst();

        while (!Objects.isNull(this.current)) {
            counter++;
            this.forward();
        }

        this.current = currentIndex;
        return counter;
    }

    public void append(T obj) {
        if (this.isEmpty()) {
            this.first = new Node<T>(obj);
            this.current = this.first;
            return;
        }

        this.toBack();
        this.current.setNext(new Node<T>(obj));
        this.current = this.current.getNext();
    }

    public void forward() {
        this.current = this.current.getNext();
    }

    public void back() {
        if (this.first == this.current) return;

        Node<T> temp = this.current;
        this.toFirst();
        while (this.current.getNext() != temp) {
            this.forward();
        }
    }

    public void toBack() {
        while (!Objects.isNull(this.current.getNext())) this.forward();
    }

    public void toFirst() {
        this.current = this.first;
    }

    public boolean isEmpty() {
        return Objects.isNull(this.first);
    }

    public T getCurrent() {
        return this.current.getContent();
    }

    public int getCurrentIndex() {
        int counter = 0;
        Node<T> temp = this.current;
        this.toFirst();
        while (this.current != temp) {
            counter++;
            this.forward();
        }
        return counter;
    }

    public int indexOf(T obj) {
        int counter = 0;
        Node<T> temp = this.current;
        this.toFirst();
        while (this.current != temp) {
            if (this.current.getContent().equals(obj)) return counter;
            counter++;
            this.forward();
        }
        return -1;
    }

    public T get(int index) {
        this.toFirst();
        for (int i = 0; i < index; i++) {
            this.forward();
        }

        return this.current.getContent();
    }

    public void insert(T obj, int index) {
        this.toFirst();
        for (int i = 0; i < index - 1; i++) {
            this.forward();
        }

        Node<T> temp = this.current.getNext();
        this.current.setNext(new Node<T>(obj));
        this.forward();
        this.current.setNext(temp);

        this.current = this.current.getNext();
    }

    public void remove(int index) {
        if (this.getLength() < index) return;

        this.toFirst();
        for (int i = 0; i < index - 1; i++) {
            this.forward();
        }

        this.current.setNext(
                Objects.isNull(this.current.getNext().getNext())
                        ? null
                        : this.current.getNext().getNext()
        );
    }

    public T pop() {
        this.toBack();
        T temp = this.current.getContent();
        this.remove(this.getCurrentIndex());
        return temp;
    }

    public T shift() {
        T temp = this.first.getContent();
        this.first = this.first.getNext();
        this.current = this.first;
        return temp;
    }

    public void print() {
        StringBuilder output = new StringBuilder("|start ");
        Node<T> currentNode = this.current;
        this.toFirst();
        while (!Objects.isNull(this.current)) {
            output.append(this.current == currentNode ? String.format("> %s(*) ", this.current.getContent()) : String.format("> %s ", this.current.getContent()));
            this.forward();
        }
        System.out.println(output);
        this.current = currentNode;
    }

    public void clear() {
        this.first = null;
        this.current = null;
    }

    public void insertFirst(T content) {
        Node<T> temp = this.first;
        Node<T> node = new Node<T>(content);
        node.setNext(temp);

        this.first = node;
        this.current = node;
    }

    public void setCurrentContent(T value) { this.current.setContent(value); }

    public void insertBefore(T content) {
        if (this.first == this.current) this.insertFirst(content);
        else this.insert(content, this.getCurrentIndex());
    }

    public void insertAfter(T content) {
        this.insert(content, this.getCurrentIndex() + 1);
    }
}