public class Main {
    public static void main(String[] args) {
        List<Integer> l = new List<Integer>();

        l.append(5);
        l.append(1);
        l.append(8);
        l.append(3);
        l.append(5);
        l.append(4);
        l.append(5);
        l.append(9);

        Selection selection = new Selection(l);
        selection.sort();
        selection.getSorted().print();
    }
}