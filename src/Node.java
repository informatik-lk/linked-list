public class Node<T> {
    private T content;
    private Node<T> next;

    public Node(T obj) {
        this.content = obj;
        this.next = null;
    }

    public T getContent() { return this.content; }

    public Node<T> getNext() { return this.next; }

    public void setNext(Node<T> value) { this.next = value; }

    public void setContent(T value) { this.content = value; }
}
