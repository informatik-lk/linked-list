public class Selection {
    public List<Integer> sorted, unsorted;

    public Selection(List<Integer> l) {
        this.unsorted = l;
        this.sorted = new List<Integer>();
    }

    public void sort() {
        while (!unsorted.isEmpty()) {
            int smallestIndex = 0;
            for (int i = 0; i < unsorted.getLength(); i++) {
                if (unsorted.get(smallestIndex) > unsorted.get(i)) smallestIndex = i;
            }

            sorted.print();

            if (smallestIndex == 0) {
                sorted.append(unsorted.shift());
            } else {
                sorted.append(unsorted.get(smallestIndex));
                unsorted.remove(smallestIndex);
            }
        }
    }

    public List<Integer> getSorted() {
        return sorted;
    }

    public List<Integer> getUnsorted() {
        return unsorted;
    }
}
