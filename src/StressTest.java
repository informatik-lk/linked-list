class StressTest {
  public static void main(String[] args){
    
    List<Object> testListe = new List<Object>();
    System.out.println(testListe.isEmpty());              // true x
    System.out.println(testListe.getLength());            // 0 x
    testListe.print();                                    // |start> null x

    testListe.insertFirst("Test");
    System.out.println(testListe.isEmpty());              // false x
    System.out.println(testListe.getLength());            // 1  x
    System.out.println(testListe.getCurrentIndex() == 0);            // true x
    testListe.append(234);
    System.out.println(testListe.getCurrentIndex() == 0);            // false x
    testListe.append(126);
    testListe.print();                                    // |start> Test > 234 > (*)126 > null x
    testListe.setCurrentContent(235);    
    testListe.print();                                    // |start> Test > 234 > (*)235 > null x
    System.out.println(testListe.getCurrent());           // 235 x
    testListe.toFirst();                                  
    testListe.back();                                     // hier sollte einfach nichts passieren - steht am Anfang
    testListe.insertBefore(true);
    testListe.print();                                    // |start> (*)true > Test > 234 > 235 > null x
    testListe.insertFirst(false);
    testListe.print();                                    // |start> (*)false > true > Test > 234 > 235 > null x
    testListe.forward();
    testListe.insertAfter(2.542);
    testListe.print();                                    // |start> false > true > (*)2.542 > Test > 234 > 235 > null x
    testListe.insert("Test!!!", 3);
    testListe.print();                                    // |start> false > true > 2.542 > (*)Test!!! > Test > 234 > 235 > null x
    testListe.shift();
    testListe.print();                                    // |start> (*)true > 2.542 > Test!!! > Test > 234 > 235 > null
    testListe.pop();
    testListe.print();                                    // |start> true > 2.542 > Test!!! > Test > (*)234 > null
    testListe.back();
    testListe.remove(testListe.getCurrentIndex());
    testListe.print();                                    // |start> true > 2.542 > (*)Test!!! > 234 > null
    testListe.remove(252);
    testListe.print();                                    // |start> true > 2.542 > (*)Test!!! > 234 > null
    testListe.clear();
    testListe.print();                                    // |start> null
  }
}
